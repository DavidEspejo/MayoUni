//
//         DETERMINAR SI UNA MATRIZ ES O NO SIMETRICA
//
// IMPLEMENTACION DEL ALGORITMO RECURSIVO RECOGIDO EN LAS DIAPOSITIVAS DEL TEMA 2
//
// IMPLEMENTACION DEL ALGORITMO ITERATIVO EQUIVALENTE
//
//

#include <stdio.h>

#define ORDEN 3

// Prototipos
bool isimetrica (int [][ORDEN], int);
bool banda_simetrica(int [][ORDEN], int);
bool simetrica_iterativa (int [][ORDEN],int);
bool simetrica_iterativa_optimizada (int [][ORDEN],int);

// Funcion Principal
int main ()  {
int M[][ORDEN]={{1,2,5},
                {3,8,6},
                {5,6,9}};

// invocacion a la funcion iSIMETRICA
if (isimetrica(M,ORDEN))  printf("\n\nLA MATRIZ ES SIMETRICA\n\n");
else printf("\n\nLA MATRIZ NO ES SIMETRICA\n\n");

// invocacion a la funcion SIMETRICA_ITERATIVA
if (simetrica_iterativa(M,ORDEN))  printf("\n\nLA MATRIZ ES SIMETRICA\n\n");
else printf("\n\nLA MATRIZ NO ES SIMETRICA\n\n");

// invocacion a la funcion SIMETRICA_ITERATIVA_OPTIMIZADA
if (simetrica_iterativa_optimizada(M,ORDEN))  printf("\n\nLA MATRIZ ES SIMETRICA\n\n");
else printf("\n\nLA MATRIZ NO ES SIMETRICA\n\n");
return 0;
}

//
// Funciones
//

bool isimetrica (int M[][ORDEN], int k){
    if (k==1) return true;
    else return isimetrica(M,k-1) && banda_simetrica(M,k);
}

bool banda_simetrica(int M[][ORDEN], int k){
    int i=0;
    bool iguales=true;
    while (i < k-1 && iguales){
          i++;
          if (M[i-1][k-1]!=M[k-1][i-1]) iguales=false;
    }
    return iguales;
}

bool simetrica_iterativa (int M[][ORDEN], int kinicial){
    int k;
    bool p;

    k=kinicial;

    while (k>1) k--;

    p=true;

    while (k!=kinicial){
        k++;
        p = p && banda_simetrica(M,k);
    }
    return p;
}

bool simetrica_iterativa_optimizada (int M[][ORDEN], int kinicial){
    int k;
    bool p;

    //k=kinicial;
    //while (k>1) k--;
    k=1;
    p=true;

    while (k!=kinicial){
        k++;
        p = p && banda_simetrica(M,k);
    }
    return p;
}
