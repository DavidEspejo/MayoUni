//
//
//  IMPLEMENTACION DEL PROBLEMA DE LA MOCHILA 0/1
//
//  Los vectores P, B y X son [0..n-1], tienen dimension n,
//  de ahi que el codigo cambie con respecto al algoritmo en que
//  para acceder al peso del objeto i, no ser� P[i] sino P[i-1], idem para vector B
//
//  La funcion mochila devuelve el beneficio maximo y dentro de la funcion
//  se imprime la secuencia de decisiones

#include <stdio.h>
#include <stdlib.h>

int mochila (int, int, int *, int *);

int main()
{
int C;
int i, n;
int *P;
int *B;

printf("Introduce la capacidad de la mochila: ");
scanf("%d",&C);
printf("\nIntroduce el numero de objetos:  ");
scanf("%d",&n);

// dimensionar vectores de pesos y beneficios
P=(int *)malloc(n*sizeof(int));
B=(int *)malloc(n*sizeof(int));

printf("\n\nIntroduce los pesos de los %d objetos\n",n);
for (i=1;i<=n;i++){
   printf("\nPeso[%d]= ", i);
   scanf("%d",&P[i-1]);
   }

printf("\n\nIntroduce los beneficios de los %d objetos\n",n);
for (i=1;i<=n;i++){
   printf("\nBeneficio[%d]= ", i);
   scanf("%d",&B[i-1]);
   }

printf("\n\n\n y el beneficio maximo es:%d\n",mochila(C,n,P,B));

free(P);
free(B);
return 0;
}

int mochila (int C, int n, int *P, int *B){
int **Dec;
int **Bmax;
int *x;
int i,j;

// dimensionar el vector de decisiones como [0..n-1]
x=(int*)malloc(n*sizeof(int));

// dimensionar las matrices de decisiones y de beneficio maximo como [0..n][0..C]
Dec=(int **)malloc((n+1)*sizeof(int *));
Bmax=(int **)malloc((n+1)*sizeof(int *));


for (i=0;i<=n;i++){
   Dec[i]=(int *)malloc((C+1)*sizeof(int));
   Bmax[i]=(int *)malloc((C+1)*sizeof(int));
   }

// inicializar las matrices con los resultados de los problemas triviales
for (j=0;j<=C;j++){
      Dec[0][j]=0;
      Bmax[0][j]=0;
      }

// rellenar las matrices por filas
for (i=1;i<=n;i++)
   for (j=0;j<=C;j++){
      if (j<P[i-1]){
      Bmax[i][j]=Bmax[i-1][j];
      Dec[i][j]=0;
      }
      else {
            if (Bmax[i-1][j]>=Bmax[i-1][j-P[i-1]]+B[i-1]){
                Bmax[i][j]=Bmax[i-1][j];
                Dec[i][j]=0;
               }
             else {
                   Bmax[i][j]=Bmax[i-1][j-P[i-1]]+B[i-1];
                   Dec[i][j]=1;
                  }
           }
      }

// solucion
i=n;
j=C;
while (i!=0){
     x[i-1]=Dec[i][j];
     j=j-x[i-1]*P[i-1];
     i=i-1;
     }

// mostrar la secuencia de decisiones optima
printf("\nLa secuencia optima de decisiones es: \n\n");

for (i=1;i<n+1;i++)
   printf("\tX[%d]=%d\t",i,x[i-1]);

int resul=Bmax[n][C];

// liberar memoria
for (i=0;i<n+1;i++){
   free(Dec[i]);
   free(Bmax[i]);
   }
free(Dec);
free(Bmax);
free(x);

// retornar el beneficio maximo
return resul;
}
